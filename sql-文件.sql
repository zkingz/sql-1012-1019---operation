with temp_active_hour_table_kps as
(
 select
 a0.dt
 ,product_id
 ,mkey
 ,substr(FROM_UNIXTIME(st_time),12,2) as hour
 ,a0.device_id
 from
 (select
 dt
 ,product_id
 ,st_time
 ,device_id
 from kps_dwd.kps_dwd_dd_view_user_active
 where dt='${dt_1}'
 ) a0
 left join
 (select
 dt
 ,mkey
 ,device_id
 from kps_dwd.kps_dwd_dd_user_channels
 where dt='${dt_1}'
 ) a1
 on a0.device_id = a1.device_id
)
select 
dt
,product
,product_id
,a1.mkey
,name_cn
,hour
,status
,dau
,new
from
(select
dt
,'K-pop' as product
,product_id
,mkey
,hour
,status
,count(distinct a.device_id) as dau
,count(distinct if(b.device_id is not null,a.device_id,null)) as new
from
(select
dt
,product_id
,mkey
,hour
,device_id
,'active' as status
from temp_active_hour_table_kps
group by dt,mkey,product_id,device_id,hour
union all
select
dt
,product_id
,mkey
,min(hour) as hour
,device_id
,'first' as status
from temp_active_hour_table_kps
group by dt,mkey,product_id,device_id
) a
left join
(
select
dt
,device_id
from kps_dwd.kps_dwd_dd_fact_view_new_user
where dt='${dt_1}'
group by dt,device_id
)b on a.dt=b.dt and a.device_id = b.device_id
group by dt
-- 拆解步骤如下，请将下列横线处拆解内容补充完整
-- 以下题4个空，每空10分
1.from kps_dwd.kps_dwd_dd_view_user_active
2.where dt='${dt_1}'
3.select
4.from
5.left join
,product_id
,mkey
,hour
,status
) a1
left join
asian_channel.dict_lcmas_channel b1
on a1.mkey = b1.mkey;
